import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import WrappedHelmet from '../../components/Helmet';

function PaymentForm(props) {
  const { updateFormData, formData, classes, submit, handleBack, loading } = props;
  const disabledNext = formData.accountOwner === '' || formData.iban === '';

  return (
    <React.Fragment>
      <WrappedHelmet title={'Payment'} />
      <Typography variant="title" gutterBottom>
        Payment data
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <TextField
            required
            id="accountOwner"
            name="accountOwner"
            label="Account Owner"
            value={formData.accountOwner}
            fullWidth
            onChange={(e) => updateFormData('accountOwner', e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="iban"
            name="iban"
            label="IBAN"
            value={formData.iban}
            fullWidth
            onChange={(e) => updateFormData('iban', e.target.value)}
          />
        </Grid>
      </Grid>
      <div className={classes.buttons}>
        <Button onClick={handleBack} className={classes.button}>
            Back
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={submit}
          className={classes.button}
          disabled={disabledNext || loading}
        >
         Submit
        </Button>
      </div>
    </React.Fragment>
  );
}

export default PaymentForm;