import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import WrappedHelmet from '../../components/Helmet';

function AddressForm(props) {
  const { updateFormData, formData, classes, handleNext, handleBack } = props;

  const disabledNext = formData.street === '' || formData.houseNumber === '' || 
                        formData.zip === '' || formData.city === '';

  return (
    <React.Fragment>
      <WrappedHelmet title={'Address'}/>
      <Typography variant="title" gutterBottom>
        Address
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <TextField
            required
            id="street"
            name="street"
            label="Street"
            fullWidth
            value={formData.street}
            onChange={(e) => updateFormData('street', e.target.value)}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            id="houseNumber"
            name="houseNumber"
            label="House Number"
            fullWidth
            value={formData.houseNumber}
            onChange={(e) => updateFormData('houseNumber', e.target.value)}
          />
        </Grid>
        <Grid item xs={2} sm={4}>
          <TextField
            required
            id="zip"
            name="zip"
            label="Zip"
            value={formData.zip}
            fullWidth
            onChange={(e) => updateFormData('zipCode', e.target.value)}
          />
        </Grid>

        <Grid item xs={7} sm={5}>
          <TextField
            required
            id="city"
            name="city"
            label="City"
            value={formData.city}
            fullWidth
            onChange={(e) => updateFormData('city', e.target.value)}
          />
        </Grid>
      </Grid>
      <div className={classes.buttons}>
        <Button onClick={handleBack} className={classes.button}>
            Back
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={handleNext}
          className={classes.button}
          disabled={disabledNext}
        >
          Next
        </Button>
       </div>
    </React.Fragment>
  );
}

export default AddressForm;