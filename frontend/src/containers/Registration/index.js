import React, { Component } from 'react'
import { connect } from 'react-redux'
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';
import Header from '../../components/Header';
import PersonalDataForm from './personalDataForm';
import AddressForm from './addressForm';
import PaymentForm from './paymentForm';
import actions from '../../business/registration/actions';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3,
    },
  },
  stepper: {
    padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing.unit * 3,
    marginLeft: theme.spacing.unit,
  },
});

const steps = ['Personal data', 'Address', 'Payment information'];

export class Registration extends Component {
  getStepContent = (step) => {
    const { updateFormData, formData, classes, submitRegistration, loading } = this.props;

    switch (step) {
      case 0:
        return <PersonalDataForm 
                  handleNext={this.handleNext} 
                  classes={classes} 
                  formData={formData} 
                  updateFormData={updateFormData} 
                />;
      case 1:
        return <AddressForm                   
                  handleNext={this.handleNext} 
                  handleBack={this.handleBack}
                  classes={classes} 
                  formData={formData} 
                  updateFormData={updateFormData} 
                />;
      case 2:
        return <PaymentForm 
                  submit={submitRegistration} 
                  handleBack={this.handleBack}
                  classes={classes} 
                  formData={formData} 
                  updateFormData={updateFormData} 
                  loading={loading}
                />;
      default:
        throw new Error('Unknown step');
    }
  }
  
  handleNext = () => {
    const { nextStep } = this.props;

    nextStep();
  };

  handleBack = () => {
    const { previousStep } = this.props;

    previousStep();
  };

  handleReset = () => {
    const { resetSteps } = this.props;

    resetSteps();
  };

  render() {
    const { classes, activeStep, newRegistration, data } = this.props;
    return (
      <React.Fragment>
        <CssBaseline />
        <Header />
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Typography variant="display1" align="center">
              Registration
            </Typography>
            <Stepper activeStep={activeStep} className={classes.stepper}>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              {activeStep === steps.length ? (
                <React.Fragment>
                  <Typography variant="headline" gutterBottom>
                    Registration complete.
                  </Typography>
                  <Typography variant="subheading">
                    Here is your account data:
                    <hr />
                    <div><strong>Name:</strong> {data.firstName}</div>
                    <div><strong>Last Name:</strong> {data.lastName}</div>
                    <div><strong>Telephone:</strong> {data.telephone}</div>
                    <div><strong>Street:</strong> {data.street}</div>
                    <div><strong>House Number:</strong> {data.houseNumber}</div>
                    <div><strong>Zip Code:</strong> {data.zipCode}</div>
                    <div><strong>City:</strong> {data.city}</div>
                    <div><strong>Account Owner:</strong> {data.accountOwner}</div>
                    <div><strong>IBAN:</strong> {data.iban}</div>
                    <div><strong>Payment Data ID:</strong> {data.paymentDataId.substring(0, 40)} {data.paymentDataId.substring(40)}</div>
                  </Typography>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={newRegistration}
                    className={classes.button}
                  >
                    New Registration
                  </Button>

                </React.Fragment>
              ) : (
                <React.Fragment>
                  {this.getStepContent(activeStep)}
                </React.Fragment>
              )}
            </React.Fragment>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Registration
})

export default connect(mapStateToProps, actions)(withStyles(styles)(Registration))
