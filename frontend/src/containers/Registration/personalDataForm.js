import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import WrappedHelmet from '../../components/Helmet';

function PersonalDataForm(props) {
  const { updateFormData, formData, classes, handleNext } = props;
  const disabledNext = formData.firstName === '' || formData.lastName === '' || formData.telephone === '';
  return (
    <React.Fragment>
      <WrappedHelmet title={'Personal data'} />
      <Typography variant="title" gutterBottom>
        Personal data
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="firstName"
            name="firstName"
            label="First name"
            fullWidth
            value={formData.firstName}
            onChange={(e) => updateFormData('firstName', e.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lastName"
            label="Last name"
            fullWidth
            value={formData.lastName}
            onChange={(e) => updateFormData('lastName', e.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="telephone"
            name="telephone"
            label="Telephone"
            fullWidth
            value={formData.telephone}
            onChange={(e) => updateFormData('telephone', e.target.value)}
          />
        </Grid>

      </Grid>
      <div className={classes.buttons}>
        <Button
          variant="contained"
          color="primary"
          onClick={handleNext}
          className={classes.button}
          disabled={disabledNext}
        >
          Next
        </Button>
      </div>

    </React.Fragment>
  );
}

export default PersonalDataForm;