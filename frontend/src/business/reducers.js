import { combineReducers } from "redux";
import Registration from "./registration/reducer";

export default combineReducers({
    Registration
});
