import { all, takeEvery, put, select } from 'redux-saga/effects';
import axios from 'axios';
import actions from './actions';

const formDataSelector = (state) => state.Registration.formData;


export function* submitRegistration() {
    const formData = yield select(formDataSelector);

    try {
        const regReq = yield axios.post('/users', formData);

        if(regReq.status === 201) {
            const { data } = regReq;
            yield put({ type: actions.REGISTRATION_SUCCESS, data })
        } else {
            yield put({ type: actions.REGISTRATION_ERROR, error: 'Missing data' })
        }
    } catch (e) {
        yield put({ type: actions.REGISTRATION_ERROR, error: e.response ? e.response.data : 'No connection' })
    }
}

export default function* rootSaga() {
    yield all([
        yield takeEvery(actions.REGISTRATION_REQUEST, submitRegistration),
    ])
}