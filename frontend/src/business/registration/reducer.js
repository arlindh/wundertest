import actions from "./actions";

const initialState = {
  data: [],
  formData: {
    firstName: '',
    lastName: '',
    telephone: '',
    street: '',
    houseNumber: '',
    zipCode: '',
    city: '',
    accountOwner: '',
    iban: ''
  },
  loaded: false,
  loading: false,
  error: false,
  activeStep: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.REGISTRATION_REQUEST: {
      return {
        ...state,
        loading: true,
        loaded: false,
      }
    }
    case actions.REGISTRATION_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: false,
        data: action.data,
        activeStep: 3
      }
    }
    case actions.REGISTRATION_ERROR: {
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
        data: []
      }
    }

    case actions.UPDATE_FORM_DATA: 
      return {
        ...state,
        formData: {
          ...state.formData,
          [action.field]: action.value 
        }
      }
    case actions.NEXT_STEP: 
      return {
        ...state,
        activeStep: state.activeStep + 1
      }
    case actions.PREVIOUS_STEP: 
      return {
        ...state,
        activeStep: state.activeStep - 1
      }
    case actions.RESET_STEPS: 
      return {
        ...state,
        activeStep: 0
      }
    case actions.NEW_REGISTRATION: 
      return initialState;
    default:
      return state;
  }
};
