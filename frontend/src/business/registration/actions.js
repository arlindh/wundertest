const actions = {
    REGISTRATION_REQUEST: "REGISTRATION_REQUEST",
    REGISTRATION_SUCCESS: "REGISTRATION_SUCCESS",
    REGISTRATION_ERROR: "REGISTRATION_ERROR",
  
    UPDATE_FORM_DATA: "UPDATE_FORM_DATA",
    NEXT_STEP: "NEXT_STEP",
    PREVIOUS_STEP: "PREVIOUS_STEP",
    RESET_STEPS: "RESET_STEPS",
    NEW_REGISTRATION: "NEW_REGISTRATION",

    newRegistration: () => ({
      type: actions.NEW_REGISTRATION
    }),

    updateFormData: (field, value) => ({
      type: actions.UPDATE_FORM_DATA,
      field,
      value
    }),
    nextStep: () => ({
      type: actions.NEXT_STEP
    }),
    previousStep: () => ({
      type: actions.PREVIOUS_STEP
    }),
    resetSteps: () => ({
      type: actions.RESET_STEPS
    }),
    submitRegistration: () => ({
      type: actions.REGISTRATION_REQUEST,
    })
  };
  
  export default actions;
  