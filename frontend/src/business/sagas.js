import { all } from 'redux-saga/effects';
import registrationSagas from './registration/saga';

export default function* rootSaga() {
    yield all([
        registrationSagas(),
    ]);
}
