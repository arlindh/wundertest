import React, {
  Component
} from 'react';
import Registration from './containers/Registration';

class App extends Component {
  render() {
    return ( 
      <Registration />
    );
  }
}

export default App;