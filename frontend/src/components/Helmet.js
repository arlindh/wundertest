import React from "react";
import PropTypes from "prop-types";
import Helmet from "react-helmet";

const WrappedHelmet = props => {
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{props.title} - WunderTest</title>
    </Helmet>
  );
};

WrappedHelmet.propTypes = {
  title: PropTypes.string.isRequired
};

export default WrappedHelmet;
