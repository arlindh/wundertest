<?php

namespace App\Http\Controllers;

use App\User;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Facades\Payment;

class UserController extends BaseController
{
    public function createUser(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'telephone' => 'required',
            'street' => 'required',
            'houseNumber' => 'required',
            'zipCode' => 'required',
            'city' => 'required',
            'accountOwner' => 'required',
            'iban' => 'required',
        ]);

        $user = User::create($request->all());
        $paymentDataId = Payment::getPaymentData($user->id, $user->iban, $user->firstName, $user->lastName);
        $user->paymentDataId = $paymentDataId;
        $user->save();

        return response()->json($user, 201);
    }

    public function getAllUsers()
    {
        return response()->json(User::all());
    }
}
