<?php

namespace App\PaymentGateway;

use GuzzleHttp;

class Payment {

    public function getPaymentData($id, $iban, $firstName, $lastName)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->request('POST', 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
            'json' => ['customerId' => $id, 'iban' => $iban, 'owner' => $firstName, " ", $lastName]
        ]);
        if($res->getStatusCode() === 200) {
            $responseBody = json_decode($res->getBody(), true);
            $paymentId = $responseBody['paymentDataId'];
            return $paymentId;
        } else 
            return null;

    }
}
