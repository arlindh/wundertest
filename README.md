#Arlind Hoxha Wunder Test

##Backend
The backend is a Lumen application. To run it, install dependencies with composer and use the command "php -S localhost:8000 -t public". The database used is sqlite and it was not included in gitignore so that when you clone the repo you will have everything ready. I also did not include the .env file in gitignore to speed up the setup process (since it is a sqlite db).

##Frontend
The frontend is a React.JS application. To get itrunning, execute "yarn" and "yarn start" in the frontend directory. The styling was all taken from the Material-UI library. No styling was done by me. A list of other packages/libraries used can be found in package.json but the most important ones are: redux, saga, axios, redux-persist.

##Running it
The frontend and backend must both be running for the app to function. 
To get a list of all registration make a GET request at: localhost:8000/api/users.
The request for a new registration was done at the same endpoint but it is POST not GET. 


##Questions in the test statement
- Describe possible performance optimizations for your Code: 
Lumen is one of the fastest frameworks out there and the architecture that I used (rest API + a reactjs app that consumes it) is one of the fastest designs AFAIK. The only thing that I can think of that can improve performance, is handling the request to your API in a more efficient way. Maybe, putting the request in an async fashion (queue?) and returning the response to the registration request ASAP without waiting for the external api request to finish would be better. When the external request ends, the backend can communicate again with the frontend and notify it on the response's status (websockets?).

- Which things could be done better, than you’ve done it?
I did not do any automated testing. That would be something which had to be done. Also, I wanted to finish this in the shortest timeframe possible so I did not give much thought on how to design the frontend state management so that it would scale better in the future. I also did not give much thought to the database design (I consider database design and css my two Achilles heels). 
Also, I would use docker and separate the frontend and backend in 2 different containers. (if there were another db, not sqlite, I would add 1 container for the db). 

In general, I wanted to keep things as simple as possible since the problem statement was simple and clear.

##Timeframe
I got the test about 24 hours before the writing of this readme. I just finished everything and in total it took me about 4 hours and 30 minutes. I worked for 2 hours in the first night as soon as I got the test and the rest I did the second evening.